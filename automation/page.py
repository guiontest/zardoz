from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
import time
from locators import MainPageLocators, DashboardLocators, CreateIssueLocators, IssueDetailLocators
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


__author__ = 'giantsizegeek'


# base class for all page objects
class BasePage(object):

    def __init__(self, driver, logger):
        self.driver = driver
        self.logger = logger

    # helper method to enter text on a text edit control
    # also creates a test step entry
    def enter_text(self, locator, text, name):
        self.logger.log_test_step("Enter text for {0}: {1}".format(name, text))
        self.driver.find_element(*locator).send_keys(text)

    # helper method to click on an element after finding it with a locator
    # also creates a test step entry
    def click(self, locator, name):
        self.logger.log_test_step("Click on {0}".format(name))
        self.driver.find_element(*locator).click()

    # helper method to click on an element
    # also creates a test step entry
    def click_element(self, element, name):
        self.logger.log_test_step("Click on {0}".format(name))
        element.click()

    # gets the text element and compares it to our expected value
    # strings used in editor may have \n newline so these are
    # tokenized and searched for
    def is_text_value_set(self, locator, expected_text_value):
        element = self.driver.find_element(*locator)
        actual_text = element.text
        string_values = expected_text_value.split('\n')
        for string_value in string_values:
            if string_value:
                if string_value not in actual_text:
                    return False
        return True


# page object for the main landing page of Jira
class MainPage(BasePage):

    @property
    def get_main_landing_page_url(self):
        # some things I would do differently as part of my job -
        # not harcoding URLs, for a sample program like this, it's ok
        # but I would rather store these externally in a properties or json file
        # at least having a method gives that option to change it.
        return "http://localhost:8080/secure/Dashboard.jspa"

    # method that helps login to Jira
    def do_login(self, username, password):
        self.enter_text(MainPageLocators.USERNAME_TEXT, username, 'Username')
        self.enter_text(MainPageLocators.PASSWORD_TEXT, password, 'Password')
        self.click(MainPageLocators.LOGIN_BUTTON, 'Login')

        # wait until the main dashboard page appears
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.presence_of_element_located((By.XPATH, DashboardLocators.ASSIGNED_HEADER_PATH)))


# page object for the main dashboard page you see after logging into Jira
class DashboardPage(BasePage):
    ASSIGNED_TO_ME = "Assigned to Me"

    def is_dashboard_visible(self):
        return self.ASSIGNED_TO_ME in self.driver.page_source

    def click_on_create(self):
        self.click(DashboardLocators.TAB_CREATE, 'Create on nav bar')

    def click_on_issue(self, partial_text_issue):
        self.driver.find_element_by_partial_link_text(partial_text_issue).click()


# page object for the Create Issue screen that appears after you click Create on the dashboard
class CreateIssueDialog(BasePage):
    PRIORITY_HIGHEST = 'Highest'
    PRIORITY_HIGH = 'High'
    PRIORITY_LOW = 'Low'
    PRIORITY_LOWEST = 'Lowest'
    PRIORITY_MEDIUM = 'Medium'

    # gets the current project set in the drop down list
    def current_project(self):
        element = self.driver.find_element(*CreateIssueLocators.PROJECT_DROP_DOWN)
        return element.get_attribute("value")

    # set the desired project
    # if the default project is our desired one, do nothing, otherwise click on
    # drop down and select the project we need
    def set_project(self, project):
        project_current = self.current_project()
        self.logger.logmsg("Current project is set to: %s" % project_current)

        if project_current != project:
            # click on this control make the drop down appear
            self.click(CreateIssueLocators.PROJECT_DROP_DOWN, "Project")
            # click on the item in the drop down list
            # note that if the item you are clicking is already selected in the text control,
            # this will fail.
            self.click((By.XPATH, CreateIssueLocators.PROJECT_OPTION % project), "project: %s" % project)

    def set_priority(self, priority):
        # click on this control make the drop down appear
        self.click(CreateIssueLocators.PRIORITY_DROP_DOWN, "Priority")
        # click on the item in the drop down list
        # note that if the item you are clicking is already selected in the text control,
        # this will fail.
        self.click((By.XPATH, CreateIssueLocators.PRIORITY_OPTION % priority), "priority: %s" % priority)

    def set_environment(self, env):
        self.enter_text(CreateIssueLocators.ENVIRONMENT_TEXT, env, "Environment")

    def set_summary(self, summary):
        self.enter_text(CreateIssueLocators.SUMMARY_TEXT, summary, "Summary")

    def toggle_bullet_list_env(self):
        self.click(CreateIssueLocators.BULLET_LIST_BUTTON, "Bullet List button on Environment")

    def toggle_bullet_list_desc(self):
        # the editor toolbar controls seem to be duplicated between
        # Environment and Description. Clicking by XPATH always does the first one.
        # There are no unique IDs I could find, the only way to to find all the
        # elements matching the XPATH and click on the 2nd one.
        elements = self.driver.find_elements(*CreateIssueLocators.BULLET_LIST_BUTTON)
        self.click_element(elements[1], "Bullet List button on Description")

    def set_description(self, desc):
        self.enter_text(CreateIssueLocators.DESCRIPTION_TEXT, desc, "Description")

    def click_on_create(self):
        self.click(CreateIssueLocators.CREATE_BUTTON, "Create button")

        # wait until the sliding dialog appears with text "Issue XXX .... successfully created"
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.text_to_be_present_in_element((By.XPATH, DashboardLocators.SUCCESS_KEY), 'successfully created'))


# page object for the Create Issue screen that appears after you click Create on the dashboard
class IssueDetailPage(BasePage):

    def verify_project(self, project):
        assert self.is_text_value_set(IssueDetailLocators.PROJECT_VALUE, project), \
            "ERROR: Project does not match %s" % project

    def verify_summary(self, summary):
        assert self.is_text_value_set(IssueDetailLocators.SUMMARY_VALUE, summary), \
            "ERROR: Summary does not match %s" % summary

    def verify_priority(self, priority):
        assert self.is_text_value_set(IssueDetailLocators.PRIORITY_VALUE, priority), \
            "ERROR: Priority does not match %s" % priority

    def verify_environment(self, environment):
        assert self.is_text_value_set(IssueDetailLocators.ENVIRONMENT_VALUE, environment), \
            "ERROR: Environment does not contain %s" % environment

    def verify_description(self, description):
        assert self.is_text_value_set(IssueDetailLocators.DESCRIPTION_VALUE, description), \
            "ERROR: Description does not contain %s" % description
