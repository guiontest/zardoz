__author__ = 'giantsizegeek'


# an object that holds the profile of a test user
class TestUser(object):
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def get_username(self):
        return self._username

    def set_username(self, username):
        self._username = username

    def get_password(self):
        return self._password

    def set_password(self, password):
        self._password = password

    username = property(get_username, set_username)
    password = property(get_password, set_password)


# an object that helps retrieve test user accounts
class UserManager(object):
    # in a real production environment I would keep test user accounts
    # stored externally, in json or properties file, not hard-coded
    def get_test_user_account(self):
        return TestUser('kidflash+test@gmail.com', '5WATu8WgkTcV')