__author__ = 'giantsizegeek'

from selenium.webdriver.common.by import By


# contains locator queries for objects on the main landing page
class MainPageLocators(object):
    USERNAME_TEXT = (By.ID, 'login-form-username')
    PASSWORD_TEXT = (By.ID, 'login-form-password')
    LOGIN_BUTTON = (By.ID, 'login')


# contains locator queries for objects on the Dashboard page
class DashboardLocators(object):
    ASSIGNED_HEADER_PATH = "//h3[contains(.,'Assigned to Me')]"
    TAB_CREATE = (By.ID, 'create_link')
    SUCCESS_KEY = "//div[@class='aui-message aui-message-success success closeable shadowed aui-will-close']"


# contains locator queries for objects on the Create Issue dialog
class CreateIssueLocators(object):
    PROJECT_TEXT = (By.ID, 'project')
    PROJECT_DROP_DOWN = (By.ID, 'project-field')
    PROJECT_OPTION = "//a[@title='%s']"
    SUMMARY_TEXT = (By.ID, 'summary')
    PRIORITY_DROP_DOWN = (By.ID, 'priority-field')
    PRIORITY_OPTION = "//a[contains(.,'%s')]"
    ENVIRONMENT_TEXT = (By.ID, 'environment')
    DESCRIPTION_TEXT = (By.ID, 'description')

    CREATE_BUTTON = (By.ID, 'create-issue-submit')

    # editor toolbar controls
    BULLET_LIST_BUTTON = (By.XPATH, "//span[contains(.,'Bullet list')]")


# contains locator queries for an issue detail page
class IssueDetailLocators(object):
    PROJECT_VALUE = (By.ID, 'project-name-val')
    SUMMARY_VALUE = (By.ID, 'summary-val')
    PRIORITY_VALUE = (By.ID, 'priority-val')
    ENVIRONMENT_VALUE = (By.ID, 'environment-val')
    DESCRIPTION_VALUE = (By.ID, 'description-val')


# contains names of projects used
class ProjectValues(object):
    PROJECT_ACCELERATOR = 'Particle Accelerator (PA)'
    PROJECT_ACCELERATOR_SHORT_NAME = 'Particle Accelerator'

