import time

__author__ = 'giantsizegeek'


class IssueUtils(object):

    SUMMARY_TEXT = "Our house is on fire!"
    DESC_TEXT1 = "I just discovered that my house is not fireproof! This is a priority 0 bug. Steps to reproduce:\n"
    DESC_TEXT2 = "I sprayed gasoline on the roof.\nLit a match.\nFire engulfed my dream home."
    ENV_TEXT = "Windows 95\nOffice 95"

    @staticmethod
    def get_issue_title(title):
        str_time = str(long(time.time() * 1000))
        return "%s:%s" % (str_time, title)