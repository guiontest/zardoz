from issueutils import IssueUtils
from locators import ProjectValues
from testlogger import TestLogger
from usermanager import UserManager

__author__ = 'giantsizegeek'

import unittest
from selenium import webdriver
import page


# test class containing methods to test creating Jira tickets
class JiraCreateTicketTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.logger = TestLogger()

    def test_search_in_python_org(self):

        # go to the main Jira page and login with test account
        main_page = page.MainPage(self.driver, self.logger)
        self.driver.get(main_page.get_main_landing_page_url)

        # get a test user account and do the login test with that
        user_account = UserManager().get_test_user_account()
        main_page.do_login(user_account.username, user_account.password)

        # verify that we are on the dashboard page after login
        dashboard_page = page.DashboardPage(self.driver, self.logger)
        assert dashboard_page.is_dashboard_visible(), "ERROR: Dashboard page not visible"

        # click on Create on the tab menu
        dashboard_page.click_on_create()

        # we are going to create a unique issue summary title with a timestamp
        unique_summary = IssueUtils.get_issue_title(IssueUtils.SUMMARY_TEXT)

        create_issue_dialog = page.CreateIssueDialog(self.driver, self.logger)

        # make sure the particle accelerator project is selected.
        create_issue_dialog.set_project(ProjectValues.PROJECT_ACCELERATOR)

        # set a unique summary for this ticket so we can find it later
        create_issue_dialog.set_summary(unique_summary)
        # set highest priority
        create_issue_dialog.set_priority(create_issue_dialog.PRIORITY_HIGHEST)

        # use bullets when typing in environment editor
        create_issue_dialog.toggle_bullet_list_env()
        create_issue_dialog.set_environment(IssueUtils.ENV_TEXT)

        # first part of description is not under a bullet
        create_issue_dialog.set_description(IssueUtils.DESC_TEXT1)
        # toggle the bullet button
        create_issue_dialog.toggle_bullet_list_desc()
        # the rest of description text should use bullets
        create_issue_dialog.set_description(IssueUtils.DESC_TEXT2)

        # click on the Create button to create the issue
        create_issue_dialog.click_on_create()

        # now we are back on the dashboard
        # click on the link that appears here
        # notice that I created a unique summary with a millisecond timestamp
        # to avoid clicking on older issues created with this test
        dashboard_page.click_on_issue(unique_summary)

        # with the issue detail page visible
        # validate that the information we created is visible on the page
        # these methods throw assertions if something does not match
        issue_detail_page = page.IssueDetailPage(self.driver, self.logger)
        issue_detail_page.verify_project(ProjectValues.PROJECT_ACCELERATOR_SHORT_NAME)
        issue_detail_page.verify_summary(unique_summary)
        issue_detail_page.verify_priority(create_issue_dialog.PRIORITY_HIGHEST)
        issue_detail_page.verify_environment(IssueUtils.ENV_TEXT)
        issue_detail_page.verify_description(IssueUtils.DESC_TEXT1)
        issue_detail_page.verify_description(IssueUtils.DESC_TEXT2)

    def tearDown(self):
        self.logger.print_test_steps()
        self.driver.close()

if __name__ == "__main__":
    unittest.main()