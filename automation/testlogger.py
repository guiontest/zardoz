__author__ = 'giantsizegeek'


# a custom logger for test cases
# records each test step as it being performed
class TestLogger(object):

    def __init__(self):
        self.test_steps = []
        self.step_number = 0

    def logmsg(self, msg):
        print "{0}".format(msg)

    def log_test_step(self, step):
        self.step_number += 1
        self.test_steps.append("Step %d: %s" % (self.step_number, step.replace("\n", "\n\t")))

    def print_test_steps(self):
        print "\nTest Steps: \n"
        for step in self.test_steps:
            print step
