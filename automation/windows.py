__author__ = 'giantsizegeek'


# helper object to dump windows information about windows displayed in the browser
class WindowHelper(object):

    def __init__(self, driver, logger):
        self.driver = driver
        self.logger = logger

    def log_current_windows(self):
        print 'Current Windows available:\n'
        win_num = 1
        for handle in self.driver.window_handles:
            print 'Window Number {0}'.format(win_num)
            self.logger.logmsg("Handle = " + handle)
            self.driver.switch_to.window(handle)
            self.logger.logmsg(self.driver.title)
        print ''
        self.driver.switch_to_default_content()
