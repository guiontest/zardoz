Automation project for Atlassian JIRA

- Unfortunately I could not make use of the TST environment - it was timing out due to
too much data to load? I decided it was easier just to install JIRA locally and do some
simple configuration there. It was much faster and easier for me, but I hope it does not
cause problems for you to run my sample test.

- How I configured my local JIRA, please see the text file: "Jira Local Install Steps.txt"
and also the screenshot "Local_Jira_Project_Configuration.png"

- The automated test is written in Python with Selenium webdriver. I assume you have pip
or easy_install programs which help install packages. I did the following:

pip install selenium

Other software used:
- Python 2.7.6
- Firefox 33.1

- The test does just a few simple steps:
 - Launches the JIRA landing page
 - Logs in with kidflash+test@gmail.com account
 - Verifies text on the dashboard
 - Clicks on the Create button on the tab
 - Makes sure we are creating an issue for "Particle Accelerator" project
 - Fills out details (summary, priority, env, description) on Create Issue page
 - Clicks on Create button
 - Waits for success message then clicks on Link to the issue
 - Verifies the details (summary, priority, env, description) on Issue page
 
Files / Classes under the automation folder:
- test.py : the main test class
- page.py : the page objects
- locators.py : constants the hold the locator values for text / buttons / etc
- testlogger.py : a logging class that also keeps track of test steps
  
- To run the test on the command line do the following:
python test.py

You should see the following output on the command line:

➜  automation git:(master) ✗ python test.py
Current project is set to: Particle Accelerator (PA)

Test Steps:

Step 1: Enter text for Username: kidflash+test@gmail.com
Step 2: Enter text for Password: 5WATu8WgkTcV
Step 3: Click on Login
Step 4: Click on Create on nav bar
Step 5: Enter text for Summary: 1431313800339:Our house is on fire!
Step 6: Click on Priority
Step 7: Click on priority: Highest
Step 8: Click on Bullet List button on Environment
Step 9: Enter text for Environment: Windows 95
	Office 95
Step 10: Enter text for Description: I just discovered that my house is not fireproof! This is a priority 0 bug. Steps to reproduce:

Step 11: Click on Bullet List button on Description
Step 12: Enter text for Description: I sprayed gasoline on the roof.
	Lit a match.
	Fire engulfed my dream home.
Step 13: Click on Create button
.
----------------------------------------------------------------------
Ran 1 test in 11.720s
