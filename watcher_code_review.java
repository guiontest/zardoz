/**
 * Adds a watcher to all of the supplied issues.
 * If there is partial success, the issues which we can modify will
 * be modified and the ones we cannot will be returned in an ArrayList.
 * @param issues the list of issues to update
 * @param currentUser the user to run the operation as
 * @param watcher the watcher to add to the issues
 * @return an ArrayList<Issue> containing the issues that could not be modified
*/
public ArrayList<Issue> addWatcherToAll (final ArrayList<Issue> issues, final User
currentUser, final User watcher) {
    ArrayList<Issue> successfulIssues = new ArrayList<Issue> ();
    ArrayList<Issue> failedIssues = new ArrayList<Issue> ();
    // of course I could point out things like not checking objects like issues
    // for null, but I assume your style is to let Java throw an exception in those
    // cases and deal with it later.
    for (Issue issue : issues) {
        if (canWatchIssue(issue, currentUser, watcher)) {
            successfulIssues.add (issue);
		}
		else {
            failedIssues.add (issue);
        }
    }
    // LOGIC BUG - should be the opposite, if not empty
    if (!successfulIssues.isEmpty()) {
    	// where does watcherManager come from? is it a member variable?
    	// I would prefer to see a method used
    	// it would a better pattern and easier to mock for unit tests
    	// getWatcherManager().startWatching (currentUser, successfulIssues);
        watcherManager.startWatching (currentUser, successfulIssues);
    }
    return failedIssues;
}

// possible bug or just a code style thing, should these params be final?
private boolean canWatchIssue (final Issue issue, final User currentUser, final User watcher) {
	// LOGIC BUG - It seems to me it should not matter if the currentUser == watcher
	// all that matters is that the currentUser has permission to modify watcher list
    if (currentUser.getHasPermissionToModifyWatchers()) {
        return issue.getWatchingAllowed (watcher);
    }
    // LOGIC BUG - default return should be false if above criteria not met
    return false;
}
